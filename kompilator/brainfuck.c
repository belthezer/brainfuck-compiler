#include<stdio.h>
#include<stdlib.h>
#define LINESZ 1024

char * parsuj();
void compile();
char *nazwaPliku;
char *nazwaPlikuWynikowego;

int main(int argc, char *argv[] )
{
	if (argv[1] == NULL) {
		printf("ERROR: Nie podano pliku jako argumentu.\n");
		return(0);
	}
	//magia <3
	int dlugosc = (int)strlen(argv[1]);
	char plik1[dlugosc];
	char plik2[dlugosc];
	strcpy(plik1,argv[1]);
	strcpy(plik2,argv[1]);
	char *rozszerzenie = plik1;
	nazwaPliku = plik2;
	rozszerzenie+=dlugosc-3;
	
	char nazwa[strlen(argv[1]-150)];
	strncpy(nazwa, argv[1], strlen(argv[1])-3);
	nazwaPlikuWynikowego=nazwa;
	
	//printf("%s",nazwa);
	
	//printf("%s, aj aj", plik);
	
	if (strcmp(rozszerzenie, ".bf") == 0) {
		FILE *f;
		char *sparsowanyText = parsuj();
		f = fopen("buffer.c", "w+"); /*create the new file*/
		if (f == NULL) {
			printf("ERROR: Wystąpił błąd podczas kompilacji. Jesteś rootem?\n");
			return(0);
		} else {
			//printf("Wynikowy texts %s", sparsowanyText);
			fprintf(f, sparsowanyText);
			fclose(f);
			compile();
			remove("buffer.c");
		}
	} else {
		printf("ERROR: %s jest złym rozszerzeniem pliku. Powinno być bf.\n", rozszerzenie);
		return(0);
	}
	//printf(" %d\n",(int)strlen(plik));
	return(0);
}
void compile() {
	//tutaj jest polecenie kompilacji <3
	char polecenie[100] ="gcc buffer.c -o ";
	strcat(polecenie, nazwaPlikuWynikowego);
	//strcat(polecenie, " -o ");
	//strcat(polecenie, "b");
	system(polecenie);
	printf("DONE!\n");
}
char * parsuj() {
	FILE *f1;
	f1 = fopen(nazwaPliku, "r");
	char buff[LINESZ];
	if (f1 == NULL) {
		printf("ERROR: Nie znaleziono pliku %s\n",nazwaPliku);
		exit(0);
	} else {
		char kodProgramu[8192] = "#include<stdio.h>\nint main(){\n\tint liczby[1000][1000];\n\tint doWypisania = 0;\n\tint i=1000;\n\tint pozycja[1000];\n\tint j=1000;\n\n\twhile(i>0){\n\t\ti--;\n\t\twhile(j>0){\n\t\tj--;\n\t\tliczby[i][j]=0;\n\t\t}\n\tpozycja[i] = 0;\n\t}\n\ti=0;\n\tj=0;\n";
		char linia[20] = "";
		int plus = 0;
		int minus = 0;
		int lewo = 0;
		int prawo = 0;
		int poziom = 0;
		int pozycja[1000];
		int i = 0;
		int numerGora = 0;
		while (i < 999) {
			i++;
			pozycja[i] = 0;
		}
		while (fgets (buff, LINESZ, f1)) {
			int i = (int)strlen(buff);
			int lineSize = (int)strlen(buff);
			while (i>0) {
				char litera = buff[lineSize-i];
				//printf("%d",i);
				
				if (minus > 0 && litera != '-') {
					sprintf(linia, "-=%d;\n", minus);
					strcat(kodProgramu, "\tliczby[i][j]");
					strcat(kodProgramu, linia);
					minus = 0;
				}
				if (plus > 0 && litera != '+') {
					sprintf(linia, "+=%d;\n", plus);
					strcat(kodProgramu, "\tliczby[i][j]");
					strcat(kodProgramu, linia);
					plus = 0;
				}
				if (prawo > 0 && litera != '>') {
					if (poziom == 0) {
						sprintf(linia, "+=%d;\n", prawo);
						strcat(kodProgramu, "\ti");
						strcat(kodProgramu, linia);
					}
					if (poziom == 1) {
						sprintf(linia, "+=%d;\n", prawo);
						strcat(kodProgramu, "\tj");
						strcat(kodProgramu, linia);
						
						sprintf(linia, "+=%d;\n", prawo);
						strcat(kodProgramu, "\tpozycja[i]");
						strcat(kodProgramu, linia);
					}
					prawo = 0;
				}
				if (lewo > 0 && litera != '<') {
					if (poziom == 0) {
						sprintf(linia, "-=%d;\n", lewo);
						strcat(kodProgramu, "\ti");
						strcat(kodProgramu, linia);
					}
					if (poziom == 1) {
						sprintf(linia, "-=%d;\n", lewo);
						strcat(kodProgramu, "\tj");
						strcat(kodProgramu, linia);
						
						sprintf(linia, "-=%d;\n", lewo);
						strcat(kodProgramu, "\tpozycja[i]");
						strcat(kodProgramu, linia);
					}
					lewo = 0;
				}
				
				if (litera == '+' || i == 1) {
					if (i!=1) plus++;
				}
				if (litera == '-' || i == 1) {
					if (i!=1) minus++;
				}
				if (litera == '>' || i == 1) {
					if (i!=1) prawo++;
					if (poziom == 1) pozycja[numerGora]++;
					if (poziom == 0) numerGora++;
				}
				if (litera == '<' || i == 1) {
					if (i!=1) lewo++;
					if (poziom == 1) pozycja[numerGora]--;
					if (poziom == 0) numerGora--;
				}
				if (litera == '.') {
					strcat(kodProgramu, "\tputchar(liczby[i][j]);\n");
				}
				if (litera == ',') {
					strcat(kodProgramu, "\tliczby[i][j]=getchar();\n");
				}
				if (litera == '[') {
					strcat(kodProgramu, "\twhile(liczby[i][j]>0){\n");
				}
				if (litera == ']') {
					strcat(kodProgramu, "\t}\n");
				}
				if (litera == 'Y') {
					if (poziom == 0) {
						poziom = 1;
						//strcat(kodProgramu, "\tj=0;\n");
						//sprintf(linia, "j=%d;\n", pozycja[numerGora]);
						strcat(kodProgramu, "\tj = pozycja[i];\n");
					}
				}
				if (litera == 'X') {
					if (poziom == 1) {
						poziom = 0;
						//strcat(kodProgramu, "\tj=0;\n");
						//sprintf(linia, "j=%d;\n", pozycja[numerGora]);
						strcat(kodProgramu, "\tj=0;\n");
					}
				}
				
				if (litera == 'W') {
					strcat(kodProgramu, "\tdoWypisania=liczby[i][j];\n\tif(doWypisania==0)putchar(48);\n\twhile(doWypisania>0){\n\t\tputchar(doWypisania%10+48);\n\t\tdoWypisania/=10;\n\t}\n\tputchar(10);\n\n");
				}
				if (litera == 'P') {
					strcat(kodProgramu, "\tdoWypisania=i;\n\tputchar(120);\n\tif(doWypisania==0)putchar(48);\n\twhile(doWypisania>0){\n\t\tputchar(doWypisania%10+48);\n\t\tdoWypisania/=10;\n\t}\n\n");
					strcat(kodProgramu, "\tdoWypisania=j;\n\tputchar(121);\n\tif(doWypisania==0)putchar(48);\n\twhile(doWypisania>0){\n\t\tputchar(doWypisania%10+48);\n\t\tdoWypisania/=10;\n\t}\n\tputchar(10);\n\n");
				}
				i--;
			}
		}

		//int liczby[100];
		//int liczba = 50;
		//char c = getchar();
		//liczby[liczba] = c;
		//printf("%i", liczby[liczba]);
		
		fclose(f1);
		strcat(kodProgramu, "}");
		return kodProgramu;
	}
	return "";
}


















